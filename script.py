#!/usr/bin/python

import subprocess as sp


def run_docker():
    sp.check_call(["docker", "run", "-it", "ubuntu:19.04"])


if __name__ == "__main__":
    run_docker()
